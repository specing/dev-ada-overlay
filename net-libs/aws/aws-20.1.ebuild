# Copyright 2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="5"

GIT_ECLASS="git-r3"
EGIT_REPO_URI="https://github.com/AdaCore/aws.git"
EGIT_BRANCH="20.1"

inherit eutils flag-o-matic toolchain-funcs ${GIT_ECLASS}

DESCRIPTION="Ada Web Server"
HOMEPAGE="https://docs.adacore.com/aws-docs/aws/index.html https://github.com/AdaCore/aws"

LICENSE="GPLv3" # GMGPL? There is a runtime exception...
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~amd64-fbsd ~sparc-fbsd ~x86-fbsd"
IUSE="+openssl +ipv6"

DEPEND="openssl? ( dev-libs/openssl )"

#RDEPEND?

S="${WORKDIR}/$PN-$PV"

PATCHES=(
	"${FILESDIR}/${PN}-20.1-fix_build.patch"
)

src_prepare() {
	epatch "${PATCHES[@]}"
}

src_configure() {
	options=()

	if use openssl; then
		options+=("SOCKET=openssl")
	fi

	if use ipv6; then
		options+=("NETLIB=ipv6")
	fi

	make "${options[@]}" setup
}

src_compile() {
	make build
}
